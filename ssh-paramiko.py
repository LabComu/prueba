import paramiko
from scp import SCPClient
import time
import serial
import os

#subir a gitlab

def actualizar():
    os.system("git add ssh-paramiko.py")
    os.system('git commit -m "actualizar"')
    os.system("git push origin master")
    print("\n-----ACTUALIZADO-----\n")

## Crea el cliente

def init_client():
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='172.24.101.41', port=2222, username='ssh', password='ssh')

##para descargar archivos del cel

def datos():
    scp = SCPClient(ssh.get_transport())
    scp.get('storage/emulated/0/Android/data/com.mendhak.gpslogger/files/gpsloggerS.csv','/home/pi/prueba/datos.csv')
    scp.close()

##transmision

def tx():
          
    ser = serial.Serial(

        port='/dev/ttyS0',
        baudrate = 9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
    )

    archivo = open("coordenadas1.csv").read()
 
   # for word in archivo:
    tam = 0
    for letra in archivo:
        tam += 1
        if letra == '\n':
            #print('k' + letra)
            ser.write('k' + str(tam) + 'k' + letra)
            print(tam)
            tam=0 
        else:
            #print (letra)   
            ser.write(letra)

        time.sleep(0.05)
       
def main():
    actualizar()
    #init_client()
    #datos()
    tx()

#########
ssh=paramiko.SSHClient()
main()
